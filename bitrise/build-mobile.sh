#!/usr/bin/env bash
# fail if any commands fails
set -e

# debug log
# set -x

# ---
PLATFORM=$1
echo "platform is = '$PLATFORM'"
if [ "$PLATFORM" != "ios" ] && [ "$PLATFORM" != "android" ] 
then
  echo "Invalid platform! Must be 'ios' or 'android'"
  exit 1
fi

# ---
BRANCH=$BITRISE_GIT_BRANCH
TAG=$BITRISE_GIT_TAG

echo "Bitrise param BITRISE_GIT_BRANCH='$BRANCH'"
echo "Bitrise param BITRISE_GIT_TAG='$TAG'"

STAGENAME=""
TRIGGER=""

if [ -z $TAG ]
then
  # All untagged builds are built for "development"
  STAGENAME="development"
  TRIGGER="Branch=$BRANCH"
else
  TAG_REGEX="^(demo|havensight-demo)/[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]/[0-9][0-9].[0-9][0-9].[0-9][0-9]$"
  if [[ $TAG =~ $TAG_REGEX ]]
  then
    STAGENAME=$(echo $TAG | sed -e 's/^\([[:alnum:]][-[:alnum:]]*\)\/.*/\1/' -e 'y/[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/[abcdefghijklmnopqrstuvwxyz]/')
    TRIGGER="Tag=$TAG"
  fi

  if [ -z $STAGENAME ]
  then
    echo "Invalid tag! Must be a known tag. Please check 'scripts/tagForBuild.sh' for valid values."
    exit 1
  fi
fi

echo "Build triggered by '$TRIGGER'"

# Expose these so we can use them in the Bitrise Slack step
envman add --key SLACK_TRIGGER --value $TRIGGER
envman add --key SLACK_STAGENAME --value $STAGENAME

# ---
echo "Getting versions"
echo "node = `node -v`"
echo "npm = `npm -v`"
if [ "$PLATFORM" = "ios" ]
then
  echo "xcode = `xcodebuild -version`"
fi

echo "Installing dependencies"
npm ci

echo "Webpack-ing the app against '$STAGENAME' environment"
# need extra heap or else it will fail on Bitrise
echo ">>> node --max_old_space_size=8192 ./node_modules/webpack/bin/webpack --mode production --env.MIRATA_CONFIGURATION=$STAGENAME"

echo "Sync-ing the iOS project"
echo ">>> npx cap sync $PLATFORM"

npm ci
npm run build:ios
